import axios from '@/libs/api.request'
import {
    getToken
} from '@/libs/util'

// 查 start
export const getAccountingData = (page = 1, pageSize = 10000000000000) => {
    return axios.request({
        url: '/api/accounting/consume/index.php',
        method: 'post',
        data: {
            page,
            pageSize
        },
        headers: {
            'Authorization': getToken()
        }
    })
}

export const deleteAccountingData = (id) => { // 删除
    id = parseInt(id)
    return axios.request({
        url: '/api/accounting/consume/del.php',
        data: {
            id
        },
        method: 'post',
        headers: {
            'Authorization': getToken()
        }
    })
}




export const editAccountingData = (data) => { // 编辑
    return axios.request({
        url: '/api/accounting/consume/edit.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

export const addAccountingData = (data) => { // 添加
    return axios.request({
        url: '/api/accounting/consume/add.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

// 域名管理 end