import axios from '@/libs/api.request'
import {
    getToken
} from '@/libs/util'

// 查 start
export const getIncomeData = (page = 1, pageSize = 10000000000000) => {
    return axios.request({
        url: '/api/accounting/income/index.php',
        method: 'post',
        data: {
            page,
            pageSize
        },
        headers: {
            'Authorization': getToken()
        }
    })
}

export const deleteIncomeData = (id) => { // 删除
    id = parseInt(id)
    return axios.request({
        url: '/api/accounting/income/del.php',
        data: {
            id
        },
        method: 'post',
        headers: {
            'Authorization': getToken()
        }
    })
}




export const editIncomeData = (data) => { // 编辑
    return axios.request({
        url: '/api/accounting/income/edit.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

export const addIncomeData = (data) => { // 添加
    return axios.request({
        url: '/api/accounting/income/add.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

// 域名管理 end