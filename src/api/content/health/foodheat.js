import axios from '@/libs/api.request'
import {
    getToken
} from '@/libs/util'

// 查 start
export const getFoodHeatData = (page = 1, pageSize = 10000000000000) => {
    return axios.request({
        url: '/api/health/foodheat/index.php',
        method: 'post',
        data: {
            page,
            pageSize
        },
        headers: {
            'Authorization': getToken()
        }
    })
}

export const deleteFoodHeatData = (id) => { // 删除
    id = parseInt(id)
    return axios.request({
        url: '/api/health/foodheat/del.php',
        data: {
            id
        },
        method: 'post',
        headers: {
            'Authorization': getToken()
        }
    })
}




export const editFoodHeatData = (data) => { // 编辑
    return axios.request({
        url: '/api/health/foodheat/edit.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

export const addFoodHeatData = (data) => { // 添加
    return axios.request({
        url: '/api/health/foodheat/add.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

// 域名管理 end