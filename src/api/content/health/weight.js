import axios from '@/libs/api.request'
import {
    getToken
} from '@/libs/util'

// 查 start
export const getWeightData = (page = 1, pageSize = 10000000000000) => {
    return axios.request({
        url: '/api/health/weight/index.php',
        method: 'post',
        data: {
            page,
            pageSize
        },
        headers: {
            'Authorization': getToken()
        }
    })
}

export const deleteWeightData = (id) => { // 删除
    id = parseInt(id)
    return axios.request({
        url: '/api/health/weight/del.php',
        data: {
            id
        },
        method: 'post',
        headers: {
            'Authorization': getToken()
        }
    })
}




export const editWeightData = (data) => { // 编辑
    return axios.request({
        url: '/api/health/weight/edit.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

export const addWeightData = (data) => { // 添加
    return axios.request({
        url: '/api/health/weight/add.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

// 域名管理 end