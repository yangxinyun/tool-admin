import axios from '@/libs/api.request'
import {
    getToken
} from '@/libs/util'

// 域名管理 start
export const getDailyPaper = (page = 1, pageSize = 1000) => {
    return axios.request({
        url: '/api/luggage/dailypaper/index.php',
        method: 'post',
        data: {
            page,
            pageSize
        },
        headers: {
            'Authorization': getToken()
        }
    })
}

export const deleteDailypaperData = (id) => { // 删除
    id = parseInt(id)
    return axios.request({
        url: '/api/luggage/dailypaper/del.php',
        data: {
            id
        },
        method: 'post',
        headers: {
            'Authorization': getToken()
        }
    })
}


export const editDailypaperData = (data) => { // 编辑
    return axios.request({
        url: '/api/luggage/dailypaper/edit.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

export const addDailypaperData = (data) => { // 添加
    return axios.request({
        url: '/api/luggage/dailypaper/add.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

// 域名管理 end