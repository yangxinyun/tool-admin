import axios from '@/libs/api.request'
import {
    getToken
} from '@/libs/util'

// 域名管理 start
export const getLuggagesData = (page = 1, pageSize = 1000) => {
    return axios.request({
        url: '/api/luggage/notepad/index.php',
        method: 'post',
        data: {
            page,
            pageSize
        },
        headers: {
            'Authorization': getToken()
        }
    })
}

export const deleteLuggageData = (id) => { // 删除
    id = parseInt(id)
    return axios.request({
        url: '/api/luggage/notepad/del.php',
        data: {
            id
        },
        method: 'post',
        headers: {
            'Authorization': getToken()
        }
    })
}

export const editLuggageData = (data) => { // 编辑
    return axios.request({
        url: '/api/luggage/notepad/edit.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

export const addLuggageData = (data) => { // 添加
    return axios.request({
        url: '/api/luggage/notepad/add.php',
        method: 'post',
        data: data,
        headers: {
            'Authorization': getToken()
        }
    })
}

// 域名管理 end