import axios from '@/libs/api.request'
import {
    getToken
} from '@/libs/util'

// 域名管理 start
export const getTodoListData = (page = 1, pageSize = 1000) => {
    return axios.request({
        url: '/api/todolist/index.php',
        method: 'post',
        data: {
            page,
            pageSize
        },
        headers: {
            'Authorization': getToken()
        }
    })
}

export const addTodoListData = (data) => {
    return axios.request({
        url: '/api/todolist/add.php',
        method: 'post',
        data,
        headers: {
            'Authorization': getToken()
        }
    })
}

export const finishTodoList = (id, value) => {
    return axios.request({
        url: '/api/todolist/finish.php',
        method: 'post',
        data: {
            id,
            value
        },
        headers: {
            'Authorization': getToken()
        }
    })
}

export const deleteTodoListData = (id) => {
    return axios.request({
        url: '/api/todolist/del.php',
        method: 'post',
        data: {
            id
        },
        headers: {
            'Authorization': getToken()
        }
    })
}

export const editTodoListData = (data) => {
    return axios.request({
        url: '/api/todolist/edit.php',
        method: 'post',
        data,
        headers: {
            'Authorization': getToken()
        }
    })
}

// 域名管理 end