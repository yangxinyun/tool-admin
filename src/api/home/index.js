import axios from '@/libs/api.request'
import {
    getToken
} from '@/libs/util'

export const getHomeData = (data) => {
    return axios.request({
        url: '/api/home/home.php',
        method: 'post',
        data,
        headers: {
            'Authorization': getToken()
        }
    })
}

export const getLuggagesCount = () => {
    return axios.request({
        url: '/api/luggages/count.php',
        method: 'post',
        headers: {
            'Authorization': getToken()
        }
    })
}

export const getTodoListCount = () => {
    return axios.request({
        url: '/api/todolist/count.php',
        method: 'post',
        headers: {
            'Authorization': getToken()
        }
    })
}

