import axios from '@/libs/api.request'
import { getToken } from '@/libs/util'

// 用户管理 start
export const getUserData = (page = 1, pageSize = 10) => {
  return axios.request({
    url: '/user/index.php',
    method: 'post',
    data: { page, pageSize },
    headers: {
      'Authorization': getToken()
    }
  })
}

export const addUserData = (data) => { // 添加
  return axios.request({
    url: '/user/add.php',
    method: 'post',
    data: data,
    headers: {
      'Authorization': getToken()
    }
  })
}

export const editUserData = (data) => { // 编辑
  return axios.request({
    url: '/user/edit.php',
    method: 'post',
    data: data,
    headers: {
      'Authorization': getToken()
    }
  })
}

export const deleteUserData = (id) => { // 删除
  return axios.request({
    url: '/user/del.php',
    data: { id },
    method: 'post',
    headers: {
      'Authorization': getToken()
    }
  })
}

// 用户管理 end
