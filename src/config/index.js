const os = require('os'); //提供本地的相关信息
// 获取本地ip
function getNetWorkIp() {
   // 打开host
   let needHost = '192.168.1.252';
   try {
      let network = os.networkInterfaces();
      for (const dev in network) {
         let iface = network[dev];
         for (let i = 0; i < iface.length; i++) {
            const alias = iface[i];
            if (
               alias.family === 'IPv4' &&
               alias.address !== '127.0.0.1' &&
               !alias.internal
            ) {
               needHost = alias.address;
            }
         }
      }
   } catch (error) {
      needHost = 'http://localhost';
   }
   return needHost
}
const IP = getNetWorkIp();


export default {
   /**
      * @description 配置显示在浏览器标签的title
      */
   title: '综合管理系统',
   /**
      * @description token在Cookie中存储的天数，默认1天
      */
   cookieExpires: 1,
   /**
      * @description 是否使用国际化，默认为false
      *              如果不使用，则需要在路由中给需要在菜单中展示的路由设置meta: {title: 'xxx'}
      *              用来在菜单中显示文字
      */
   useI18n: false,
   /**
      * @description api请求基础路径
      */
   baseUrl: {
      dev: 'http://' + IP + '/tool-admin-server/',
      pro: 'http://' + IP + '/tool-admin-server/'
   },
   /**
      * @description 默认打开的首页的路由name值，默认为home
      */
   homeName: 'home',
   /**
      * @description 需要加载的插件
      */
   plugin: {
      'error-store': {
         showInHeader: true, // 设为false后不会在顶部显示错误日志徽标
         developmentOff: true // 设为true后在开发环境不会收集错误信息，方便开发中排查错误
      }
   }
}
