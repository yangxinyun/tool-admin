import Main from '@/components/main'
export default {
    path: '/accounting',
    name: 'accounting',
    meta: {
        icon: 'logo-yen',
        title: '财务管理'
    },
    component: Main,
    children: [{
        path: 'statistics',
        name: 'statistics',
        meta: {
            icon: 'ios-podium-outline',
            title: '收支统计'
        },
        component: () =>
            import('@/view/components/accounting/AccountingDataView.vue')
    },{
        path: 'index',
        name: 'index',
        meta: {
            icon: 'logo-usd',
            title: '消费记录'
        },
        component: () =>
            import('@/view/components/accounting/Accounting.vue')
    },{
        path: 'income',
        name: 'income',
        meta: {
            icon: 'ios-calendar-outline',
            title: '收入记录'
        },
        component: () =>
            import('@/view/components/accounting/Income.vue')
    }]
}