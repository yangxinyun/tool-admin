import Main from '@/components/main'
export default {
    path: '/health',
    name: 'health',
    meta: {
        icon: 'ios-heart-outline',
        title: '健康监测'
    },
    component: Main,
    children: [{
        path: 'weight',
        name: 'weight',
        meta: {
            icon: 'ios-body-outline',
            title: '体重监控'
        },
        component: () =>
            import('@/view/components/health/Weight.vue')
    },{
        path: 'foodheat',
        name: 'foodheat',
        meta: {
            icon: 'ios-settings-outline',
            title: '食物热量'
        },
        component: () =>
            import('@/view/components/health/FoodHeat.vue')
    }]
}