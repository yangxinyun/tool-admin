import Main from '@/components/main'
export default {
    path: '/paper',
    name: 'paper',
    meta: {
        icon: 'md-create',
        title: '每日总结'
    },
    component: Main,
    children: [{
        path: 'luggage',
        name: 'luggage',
        meta: {
            icon: 'ios-create',
            title: '日记管理'
        },
        component: () =>
            import ('@/view/components/luggage/Luggage.vue')
    },{
        path: 'dailypaper',
        name: 'dailypaper',
        meta: {
            icon: 'ios-paper-outline',
            title: '日报周报'
        },
        component: () =>
            import ('@/view/components/luggage/Dailypaper.vue')
    }]
}