import Main from '@/components/main'
export default {
    path: '/notepad',
    name: 'notepad',
    meta: {
        icon: 'ios-list-box-outline',
        title: '待办事项'
    },
    component: Main,
    children: [{
        path: 'todolist',
        name: 'todolist',
        meta: {
            icon: 'ios-list-box-outline',
            title: '待办事项'
        },
        component: () =>
            import('@/view/components/todolist/TodoList.vue')
    }]
}