import Vue from 'vue'
import Vuex from 'vuex'

import user from './module/user'
import app from './module/app'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    pageSizeArray: [5, 10, 100, 500, 1000, 10000],
    pageSize: 10,
    //折线统计图显示最近几天数据
    showCount: 30,
    homeSize: 6,
    foodType: [
      { title: "主食类", value: 1 },
      { title: "蔬菜类", value: 2 },
      { title: "水果类", value: 3 },
      { title: "肉类", value: 4 },
      { title: "蛋类", value: 5 },
      { title: "水产类", value: 6 },
      { title: "奶类", value: 7 },
      { title: "油脂类", value: 8 },
      { title: "糕点小吃类", value: 9 },
      { title: "糖类", value: 10 },
      { title: "饮料类", value: 11 },
      { title: "菌藻类", value: 12 },
      { title: "其他", value: 13 }
    ]
  },
  mutations: {
    //
  },
  actions: {
    //
  },
  modules: {
    user,
    app,
  }
})
