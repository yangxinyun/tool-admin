import {
  login
} from '@/api/user'
import { setToken, getToken } from '@/libs/util'

export default {
  state: {
    avatarImgPath: '/logo.gif',
    token: getToken()
  },
  mutations: {
    setToken(state, token) {
      state.token = token
      setToken(token)
    }
  },
  getters: {

  },
  actions: {
    // 登录
    handleLogin({ commit }, { userName, password }) {
      userName = userName.trim()
      return new Promise((resolve, reject) => {
        login({
          userName,
          password
        }).then(res => {
          const data = res.data.data
          if (res.data.code === 0) {
            commit('setToken', data)
          }
          resolve(res)
        }).catch(err => {
          reject(err)
        })
      })
    },
    // 退出登录
    handleLogOut({ state, commit }) {
      return new Promise((resolve, reject) => {
        commit('setToken', '')
        resolve()
      }).catch(err => {
        reject(err)
      })
    },

  }
}
